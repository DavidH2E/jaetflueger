// demo: CAN-BUS Shield, send data
// loovee@seeed.cc


#include <SPI.h>

#define CAN_2515
// #define CAN_2518FD

// Set SPI CS Pin according to your hardware

#if defined(SEEED_WIO_TERMINAL) && defined(CAN_2518FD)
// For Wio Terminal w/ MCP2518FD RPi Hat：
// Channel 0 SPI_CS Pin: BCM 8
// Channel 1 SPI_CS Pin: BCM 7
// Interupt Pin: BCM25
const int SPI_CS_PIN  = BCM8;
const int CAN_INT_PIN = BCM25;
#else

// For Arduino MCP2515 Hat:
// the cs pin of the version after v1.1 is default to D9
// v0.9b and v1.0 is default D10
const int SPI_CS_PIN = 9;
const int CAN_INT_PIN = 2;
#endif


#ifdef CAN_2518FD
#include "mcp2518fd_can.h"
mcp2518fd CAN(SPI_CS_PIN); // Set CS pin
#endif

#ifdef CAN_2515
#include "mcp2515_can.h"
mcp2515_can CAN(SPI_CS_PIN); // Set CS pin
#endif

// PIN assignment
const int sensorPin = A0;

// Speed config
const float voltageMin  = 5.50; // voltage corresponding to min poti setting
const float voltageMax  = 0.00; // voltage corresponding to max poti setting
const int rpmMin        = 100;  // rpm at min poti setting
const int rpmMax        = 500;  // rpm at max poti setting
const int rpmLimit      = 1000; // Software limit for rpm --> Poti failure can not lead to excessive rpm

void setup() {
    SERIAL_PORT_MONITOR.begin(115200);
    while(!Serial){};

    while (CAN_OK != CAN.begin(CAN_125KBPS)) {             // init can bus : baudrate = 500k
        SERIAL_PORT_MONITOR.println("CAN init fail, retry...");
        delay(100);
    }
    SERIAL_PORT_MONITOR.println("CAN init ok!");

    unsigned char nmt_op[2] = {0x01, 0x7F};
    CAN.sendMsgBuf(0x00, 0, 2, nmt_op);
    SERIAL_PORT_MONITOR.println("NMT set operational.");
    delay(100);

    unsigned char stmsg1[8] = {0x40, 0x1C, 0x30, 0x01, 0x00, 0x00, 0x00, 0x00};
    CAN.sendMsgBuf(0x67F, 0, 8, stmsg1);
    SERIAL_PORT_MONITOR.println("Start MSG1.");     
    delay(100);

    unsigned char stmsg2[8] = {0x2B, 0x1C, 0x30, 0x01, 0x08, 0x00, 0x00, 0x00};
    CAN.sendMsgBuf(0x67F, 0, 8, stmsg2);
    SERIAL_PORT_MONITOR.println("Start MSG2.");     
    delay(100);  

    CAN.sendMsgBuf(0x67F, 0, 8, stmsg1);
    SERIAL_PORT_MONITOR.println("Start MSG1.");     
    delay(100);

    unsigned char stmsg3[8] = {0x2B, 0x1C, 0x30, 0x01, 0x02, 0x00, 0x00, 0x00};
    CAN.sendMsgBuf(0x67F, 0, 8, stmsg3);
    SERIAL_PORT_MONITOR.println("Start MSG3.");     
    delay(100);    
}

void loop() {

  int sensorVal = analogRead(sensorPin);
  float voltage = (sensorVal / 1024.0) * 5.5;
  float rpm_perc = voltage * (100 - 0) / (voltageMax - voltageMin) + 100;  
  Serial.print("Sensor Value: ");
  Serial.print(sensorVal);
  Serial.print(", Voltage: ");
  Serial.print(voltage);
  Serial.print(" V");
  Serial.print(", Percentage: "); 
  Serial.print(rpm_perc);

  // Calculate rpm
  int rpm = rpm_perc * (rpmMax - rpmMin) / (100 - 0) + rpmMin;
  //Safety limit for rpm
  if (abs(rpm) > rpmLimit) {
    int sign = rpm / abs(rpm);   // -1 or +1
    rpm = sign * rpmLimit; 
  }

  union {
    unsigned int rpm;
    unsigned char bytes[4];
  } speedData;
  speedData.rpm = rpm;   

  unsigned char msg1[8] = {0x2B, 0x24, 0x30, 0x01, speedData.bytes[0], speedData.bytes[1], 0x00, 0x00};
  CAN.sendMsgBuf(0x67F, 0, 8, msg1);     

  SERIAL_PORT_MONITOR.print(", RPM: ");
  SERIAL_PORT_MONITOR.print(rpm);
  SERIAL_PORT_MONITOR.print(", Byte0: ");
  SERIAL_PORT_MONITOR.print(String(speedData.bytes[0], HEX));
  SERIAL_PORT_MONITOR.print(", Byte1: ");    
  SERIAL_PORT_MONITOR.println(String(speedData.bytes[1], HEX));
  delay(100); 
}

// END FILE
