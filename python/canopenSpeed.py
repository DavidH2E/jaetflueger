# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 20:36:34 2022

@author: david
"""

import canopen
import time

node_no     = 127
wait_s      = 15
eds_path    = "C:\\Users\\david\\OneDrive - H2energy\\99_Pers\\08_Jaetflueger\\ILS1F_EDS\\SEILS1F_0110E.eds"

try: 
    network = canopen.Network()
    network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=125000)
    print("Connected to network")
    
    node = network.add_node(node_no, eds_path)
    print("Node added")
    time.sleep(3)
    
    node.nmt.state = 'OPERATIONAL'
    print("Set OPERATIONAL.")
    time.sleep(wait_s)      
    
    ctrl_word   = node.sdo['settings1']['Commands.driveCtrl']
    vel         = node.sdo['vel']['VEL.velocity']
    # Reset fault
    ctrl_word.bits[3] = 1
    print("Fault reset")
    time.sleep(wait_s)      
    # Enable
    ctrl_word.bits[1] = 1
    print("Enabled")
    time.sleep(wait_s)      
    
    # Velocity setting
    vel.raw = 200
    time.sleep(1)
    print("vel: " + str(vel.phys))
    time.sleep(wait_s)
    vel.raw = -200
    time.sleep(1)
    print("vel: " + str(vel.phys))
    time.sleep(wait_s)     
    

    vel.raw = 0
    print("vel: " + str(vel.phys))
        
    # Terminate
    network.disconnect()
    print("Network disconnected.")    
        
    
except Exception as err:
    network.disconnect()
    print("Network disconnected.")
    print(str(err))


            
        